package com.mvp.thirty.commons;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import static com.mvp.thirty.commons.Constants.AGENT_CONFIG.*;

public class Constants {

    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public static int AMB_ERROR = 999;
    public static int COMMISSION_VALID_AMOUNT = 10000;
    public enum Source {
        WEB,
        MOBILE,
        ADMIN,
        GG,
        FB
    }
    public enum  ERROR
    {
        ERR_TOKEN("Your token has expired"),
        ERR_PREFIX("Invalid data"),
        ERR_IMAGE("Image file not found"),
        ERR_99999("The system cannot process."),
        ERR_88888("No right to make a transaction"),
        ERR_77777("This Account is login Other Device"),

        ERR_99001("The file cannot be accessed."),
        ERR_99002("Invalid file information"),
        ERR_99003("Can't find it"),


        ERR_00000("Unable to connect to Database"),
        ERR_00001("The information is already in the system."),
        ERR_00002("data is locked"),
        ERR_00003("username  is required"),
        ERR_00004("This username already exists."),
        ERR_00005("password  is required"),
        ERR_00006("name and lastname  is required"),
        ERR_00007("username or password Invalid"),
        ERR_00008("agent not found"),
        ERR_00009("This user,admin information could not be found."),
        ERR_00010("id  is required"),
        ERR_00011("no information found"),
        ERR_00012("This user could not be found."),

        ERR_00013("Please wait for approval to withdraw."),


        ERR_00014("Failed to withdraw"),
        ERR_00015("Another Admin is making a list."),

        ERR_00017("Please wait for approval to Deposit."),
        ERR_00016("This account has already been used, please contact admin."),

        ERR_01000("id  is required"),
        ERR_01001("ืชื่อ  is required"),
        ERR_01002("BankName  is required"),
        ERR_01003("Acoount Number  is required"),
        ERR_01004("This name does not exist in the system."),
        ERR_01005("Can't find this crook"),

        ERR_02000("bank code  is required"),
        ERR_02001("bank type  is required"),
        ERR_02002("bank name  is required"),
        ERR_02003("bank account name  is required"),
        ERR_02004("bank account no  is required"),
        ERR_02005("username  is required"),
        ERR_02006("password  is required"),
        ERR_02007("bank order  is required"),
        ERR_02008("bank group  is required"),
        ERR_02009("bot ip  is required"),
        ERR_02010("no bank information"),
        ERR_02011("no bank information"),
        ERR_02012("The bank account number must be numeric only."),
        ERR_02013("Invalid Bot Ip Number"),
        ERR_02014("invalid bank order"),
        ERR_02016("invalid bank group"),
        ERR_02018("bank group and bank order already in the system"),
        ERR_02019("Unable to delete bank because there is no new bank to replace"),

        ERR_02020("Deposit amount greater than required"),

        ERR_02021("Deposit amount less than required"),

        ERR_02022("withdraw amount greater than required"),

        ERR_02023("withdraw amount less than required"),
        ERR_01101("role code  is required"),
        ERR_01102("description  is required"),
        ERR_01103("role_code already exist"),
        ERR_01104("Id not found"),
        ERR_01105("Id  is required"),
        ERR_01106("phone number  is required"),
        ERR_01107("phone number This already exists in the system."),
        ERR_01108("password  is required"),
        ERR_01109("name  is required"),
        ERR_01110("bot ip  is required"),
        ERR_01111("bank group  is required"),
        ERR_01112("new user flag  is required"),
        ERR_01113("active  is required"),
        ERR_01114("phone number invalid"),
        ERR_O1115("หมายเลข Bot Ip invalid"),
        ERR_01116("bank group invalid"),
        ERR_01117("bank group Already in the system"),
        ERR_01118("username  is required"),
        ERR_01119("username Already in the system"),
        ERR_01120("bank name  is required"),
        ERR_01121("account number  is required"),
        ERR_01122("account number invalid"),
        ERR_01123("first name  is required"),
        ERR_01124("last name  is required"),
        ERR_01125("is bonus  is required"),
        ERR_01126("password invalid"),
        ERR_01127("username Already in the system"),
        ERR_01128("credit  is required"),
        ERR_01129("point  is required"),
        ERR_01130("prefix  is required"),
        ERR_01131("credit invalid"),
        ERR_01132("The wallet for this username was not found."),
        ERR_01133("Not enough credit"),
        ERR_01134("Fixed today's overdue bug."),
        ERR_01135("this bank account number Already in the system"),
        ERR_01136("overdue withdrawal Please make a new list tomorrow."),
        ERR_01137("Can't clear turnover"),
        ERR_01138("ufaUsername  is required"),
        ERR_01139("ufaPassword  is required"),

        ERR_01140("key  is required"),
        ERR_01141("prefix Already in the system"),
        ERR_01142("credit not enough"),
        ERR_03000("IP  is required"),
        ERR_03001("Data not found."),


        ERR_04000("data not found true wallet"),
        ERR_04001("can't edit deposit true wallet"),

        ERR_05001("Please select the period you want to search."),

        ERR_06001("name  is required"),
        ERR_06002("type bonus  is required"),
        ERR_06003("type promotion  is required"),
        ERR_06004("max bonus  is required"),
        ERR_06005("min topup  is required"),
        ERR_06006("max topup  is required"),
        ERR_06007("turn over  is required"),
        ERR_06008("max withdraw  is required"),

        ERR_06009("name do not repeat"),
        ERR_04002("Point not enough"),
        ERR_04003("Credit Insufficient, please top up"),
        ERR_04004("You have Turn over %s credit"),
        ERR_04005("Credit not enough"),
        ERR_04006("you must have Credit %s credit can withdraw"),

        ERR_04007("Agent Credit not enough"),

        ERR_04008("Commission not enough"),

        ERR_09001("BotType  is required"),
        ERR_09002("TransactionId  is required"),
        ERR_09003("BotIp  is required"),
        ERR_09004("BankCode  is required"),
        ERR_09005("BankAccountNo  is required"),
        ERR_09006("AccountNo  is required"),
        ERR_09007("Amount  is required"),
        ERR_09008("TransactionDate  is required"),
        ERR_09009("Type  is required"),
        ERR_09010("max receive bonus  is required"),
        ERR_09011("Promotion not found"),

        ERR_10001("cannot withdraw credit"),

        ERR_11001("bonus condition  is required"),
        ERR_11002("max condition  is required"),
        ERR_11003("min condition  is required"),
        ERR_11004("Invalid bonus type"),

        ERR_12001("min topup prohibit more than max topup"),
        ERR_12002("min topup is not equal to max topup"),
        ERR_12003("The min topup condition must not be less than the promotion min topup."),
        ERR_12004("conditions max topup prohibit more than promotion max topup"),

        ERR_13001("Duplicate Role Name Exists"),

        ERR_13002("RoleId Not Fount"),

        ERR_13003("Startdate And Enddate is required."),
        CORE_00000("The system succeeded."),
        CORE_11111("Unable to retrieve game list"),
        CORE_22222("Unable to retrieve details"),

        CORE_00001("Invalid request"),
        CORE_00002("Internal server error"),
        CORE_00003("Value cannot be null"),
        CORE_00004("Player isn't exist"),
        CORE_00005("Player wallet doesn't exist"),
        CORE_00006("No bet exist"),
        CORE_00007("Bet failed"),
        CORE_00008("Payout failed"),
        CORE_00009("Not enough cash balance to bet"),

        CORE_01001("Provider is not valid"),
        CORE_01002("Provider Empty or No Provider Currently Active"),

        CORE_01101("Provider Service Error"),

        CORE_02001("Session Token is not valid"),

        ;

        public String msg;

        ERROR(String label) {
            this.msg = label;
        }
    }
    public enum MESSAGE{
        MSG_00000("The system succeeded."),

        MSG_00001("successful withdrawal"),

        MSG_01000("Fetching crooks successfully"),
        MSG_01001("Successfully added crooked information"),
        MSG_01002("Successful editing of fraudulent information"),
        MSG_01003("Successful deletion of fraudulent data"),

        MSG_02000("Successfully added bank information."),
        MSG_02001("Successfully edited bank information."),
        MSG_02002("Successfully deleted bank information."),
        GRAPES("GREEN");

        public String msg;

        MESSAGE(String label) {
            this.msg = label;
        }
    }


    public enum ROLE {
        XSUPERADMIN("XSUPERADMIN"),
        SUPERADMIN("SUPERADMIN"),
        ADMIN("ADMIN"),
        STAFF("STAFF");

        private final String role;

        ROLE(final String text) {
            this.role = text;
        }

        @Override
        public String toString() {
            return role;
        }
    }

    public static class Sort {
        public static final String ASC = "A";
        public static final String DESC = "D";
    }

    public static class AGENT_CONFIG_TYPE  {
        public static final String AMB_CONFIG = "AMB-CONFIG";
    }

    public static class AGENT_CONFIG  {
        public static final String MAX_AUTO_WITHDRAW = "maxAutoWithdraw";
        public static final String MIN_WITHDRAW_CREDIT = "minWithdrawCredit";
        public static final String COUNT_WITHDRAW = "countWithDraw";
        public static final String LIMIT_WITHDRAW = "limitWithDraw";
        public static final String APPROVE_WITHDRAW_AUTO = "approveWithdrawAuto";
        public static final String APPROVE_WITHDRAW_AUTO_NEW = "approveWithdrawAutoNew";
        public static final String ON_OFF_WEBSITE = "onOffWebsite";
        public static final String FORCE_BONUS = "forceBonus";
        public static final String WINLOSE_REFUND = "winLoseRefund";
        public static final String WINLOSE_REFUND_RATE = "winLoseRefundRate";
        public static final String WINLOSE_REFUND_TYPE = "winLoseRefundType";
        public static final String WINLOSE_REFUND_TURN = "winLoseRefundTurn";
        public static final String WINLOSE_REFUND_MAX = "winLoseRefundMax";
        public static final String CHECK_BONUS_WITHDRAW = "checkBonusWithdraw";

        public static final String FORCE_WITHDRAW_ALL = "forceWithdrawAll";

        public static final String ALLOW_OUTSTANDING_BET = "allowOutstandingBet";
        public static final String TRANS_CREDIT = "TRANS_CREDIT";
        public static final String URL_CONFIG = "URL-CONFIG";

        public static final String URL_AMB_GAME = "urlAmbGame";
        public static final String URL_AMB_MOBILE_GAME = "urlMobileAmbGame";
        public static final String AMB_HASH = "ambHash";
    }

    public static List<String> AGENT_CONFIG_STATUS = Arrays.asList(LIMIT_WITHDRAW, APPROVE_WITHDRAW_AUTO, APPROVE_WITHDRAW_AUTO_NEW, ON_OFF_WEBSITE, FORCE_BONUS, WINLOSE_REFUND, CHECK_BONUS_WITHDRAW,FORCE_WITHDRAW_ALL);

    public static class DEPOSIT_STATUS {
        public static final String PENDING = "PENDING";
        public static final String SUCCESS = "SUCCESS";
        public static final String ERROR = "ERROR";

        public static final String NOT_SURE = "NOT_SURE";
        public static final String BLOCK_AUTO = "BLOCK_AUTO";
        public static final String REJECT = "REJECT";
        public static final String REJECT_N_REFUND = "REJECT_N_REFUND";
    }

    public static class WITHDRAW_STATUS {
        public static final String PENDING = "PENDING";
        public static final String SUCCESS = "SUCCESS";
        public static final String ERROR = "ERROR";

        public static final String BLOCK_AUTO = "BLOCK_AUTO";
        public static final String REJECT = "REJECT";
        public static final String REJECT_N_REFUND = "REJECT_N_REFUND";

        public static final String SELF_TRANSFER = "SELF_TRANSFER";

    }


    public static class DEPOSIT_TYPE {
        public static final String BANK = "BANK";
        public static final String TRUEWALLET = "TRUEWALLET";
    }

    public static class POINT_TRANS_STATUS {
        public static final String PENDING = "PENDING";
        public static final String SUCCESS = "SUCCESS";
        public static final String ERROR = "ERROR";
    }


    public static class POINT_TYPE {
        public static final String EARN_POINT = "EARN_POINT";
        public static final String TRANS_CREDIT = "TRANS_CREDIT";

        public static final String COMM_CREDIT = "COMM_CREDIT";
    }

    public static class PROBLEM {
        public static final String NO_SLIP = "NO_SLIP";
        public static final String CUT_CREDIT = "CUT_CREDIT";
        public static final String ADD_CREDIT = "ADD_CREDIT";
        public static final String CLEAR_TURN = "CLEAR_TURN";
    }

    public static class PROMOTION_TYPE {
        public static final String ALLDAY = "ALLDAY";
        public static final String NEWUSER = "NEWUSER";
        public static final String FIRSTTIME = "FIRSTTIME";
        public static final String GOLDTIME = "GOLDTIME";
        public static final String SEVENDAYINROW = "7DAYINROW";
    }

    public static class TURN_TYPE {
        public static final String TURNOVER = "TURNOVER";
        public static final String MINCREDIT = "MINCREDIT";
    }



    public static class AFFILIATE_TYPE {
        public static final String FIX = "FIX";
        public static final String PERCENT = "PERCENT";
    }

    public static final String MESSAGE_WITHDRAW_BLOCK = "User %s Request approval to withdraw credit quantity %s IDR";

    public static final String MESSAGE_WITHDRAW_APPROVE = "Admin %s approve withdrawal credit ให้ User %s quantity %s IDR";

    public static final String MESSAGE_WITHDRAW_REJECT = "Admin %s refuse to withdraw money ของ User %s quantity %s IDR";

    public static final String MESSAGE_WITHDRAW_REJECT_RF = "Admin %s Refuse to withdraw money and return. credit ให้ User %s quantity %s IDR";

    public static final String MESSAGE_WITHDRAW = "User %s withdraw credit quantity %s IDR";

    public static final String MESSAGE_WITHDRAW_ERROR = "User %s withdraw credit quantity %s IDR unsuccessful : %s";

    public static final String MESSAGE_WITHDRAW_SELF = "Admin %s withdraw User %s quantity %s IDR";

    public static final String MESSAGE_RE_WITHDRAW = "Admin %s withdraw User %s quantity %s IDR";

    public static final String MESSAGE_RE_WITHDRAW_ERROR = "Admin %s make a new withdrawal User %s quantity %s IDR unsuccessful : %s";

    public static final String MESSAGE_WITHDRAW_REMAIN = " balance %s IDR";

    public static final String MESSAGE_WITHDRAW_WAIT_MANUAL = "User %s withdraw credit quantity %s IDR waiting to withdraw";

    public static final String MESSAGE_DEPOSIT = "User %s Deposit credit quantity %s IDR";

    public static final String MESSAGE_DEPOSIT_REJECT = "Admin %s refuse to deposit  User %s quantity %s IDR";

    public static final String MESSAGE_DEPOSIT_REJECT_RF = "Admin %s Refused the deposit and refunded. User %s quantity %s IDR";

    public static final String MESSAGE_ADMIN_DEPOSIT = "Admin %s deposit credit User %s quantity %s IDR";

    public static final String MESSAGE_ADMIN_DEPOSIT_BONUS = "Admin %s deposit credit User %s quantity %s IDR bonus %s IDR";

    public static final String MESSAGE_SYSTEM_REFUND_WINLOSE = "The system returns the loss amount User %s from the loss %s IDR get back %s IDR";

    public static final String MESSAGE_DEPOSIT_BLOCK = "User %s request for approval credit quantity %s IDR";

    public static final String MESSAGE_POINT_TRANSFER = "User %s transfer point %s";

    public static final String MESSAGE_COMMISSION_TRANSFER = "User %s transfer value %s";

    public static final String MESSAGE_NOTSURE_REJECT = "Admin %s Refuse to deposit amount %s IDR";

    public static final String MESSAGE_ADMIN_CLEAR_TURN = "Admin %s clear turn User %s quantity %s turnover status %s";

    public static final String MESSAGE_POINT_EARN = "User %s receive %s point";
}