package com.mvp.thirty.utils;

import com.mvp.thirty.config.property.JWTProperty;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = 1L;

//    @Value("${jwt.secret}")
//    private String secret;
//
//    public String generateToken(Map<String, Object> claims, String subject) {
//        return doGenerateToken(claims, subject);
//    }
//
//    public String getDataToken(String token, String key) {
//        try {
//            final Claims claims = getAllClaimsFromToken(token);
//
//            return ObjectUtils.isEmpty(claims.get(key)) ? null : String.valueOf(claims.get(key));
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
//        final Claims claims = getAllClaimsFromToken(token);
//        return claimsResolver.apply(claims);
//    }
//
//    public String generateToken(Map<String, Object> claims, String subject, int expireMinute) {
//        final Date createdDate = new Date();
//        return Jwts.builder()
//                .setClaims(claims)
//                .setSubject(subject)
//                .setIssuedAt(createdDate)
//                .setExpiration(new Date(System.currentTimeMillis() + expireMinute * 60 * 1000 ))
//                .signWith(SignatureAlgorithm.HS512, secret)
//                .compact();
//    }
//    private String doGenerateToken(Map<String, Object> claims, String subject) {
//        final Date createdDate = new Date();
//        Map<String, Object> header = new HashMap<>();
//        header.put("typ", "jwt");
//        return Jwts.builder()
//                .setClaims(claims)
//                .setSubject(subject)
//                .setIssuedAt(createdDate)
//                .setHeader(header)
////                .setExpiration(expirationDate)
//                .signWith(
//                        SignatureAlgorithm.HS512,
//                        secret
//                )
//                .compact();
//    }
//
//    private Claims getAllClaimsFromToken(String token) {
//        return Jwts.parser()
//                .setSigningKey(secret)
//                .parseClaimsJws(token)
//                .getBody();
//    }

    @Autowired
    private JWTProperty jwtProperty;

    public String generateToken(Map<String, Object> claims, String subject) {
        return doGenerateToken(claims, subject);
    }

    public String getDataToken(String token, String key) {
        final Claims claims = getAllClaimsFromToken(token);
        return String.valueOf(claims.get(key));
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    public String generateToken(Map<String, Object> claims, String subject, int expireMinute) {
        final Date createdDate = new Date();
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(createdDate)
                .setExpiration(new Date(System.currentTimeMillis() + expireMinute * 60 * 1000 ))
                .signWith(SignatureAlgorithm.forName(jwtProperty.getAlgorithm()), jwtProperty.getSecret().getBytes())
                .compact();
    }

    private String doGenerateToken(Map<String, Object> claims, String subject) {
        final Date createdDate = new Date();
        Map<String, Object> header = new HashMap<>();
        header.put("typ", "jwt");
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(createdDate)
                .setHeader(header)
                .setExpiration(new Date(System.currentTimeMillis() + 43200000))
                .signWith(
                        SignatureAlgorithm.forName(jwtProperty.getAlgorithm()),
                        jwtProperty.getSecret().getBytes()
                )
                .compact();
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(jwtProperty.getSecret().getBytes())
                .parseClaimsJws(token)
                .getBody();
    }

    public Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

}
