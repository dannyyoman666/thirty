package com.mvp.thirty.interceptor;

import com.mvp.thirty.service.TokenAuthenticationService;
import com.mvp.thirty.utils.MultiReadRequest;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

@Component
@Slf4j
public class JwtAuthorizationTokenFilter extends OncePerRequestFilter {


    @Autowired
    private TokenAuthenticationService tokenAuthenticationService;

    private final AntPathMatcher pathMatcher = new AntPathMatcher();

    private final Collection<String> excludeUrlPatterns = new ArrayList<>(
            Arrays.asList(
                    "/api/admin/auth",
                    "/api/user/auth",
                    "/api/user/register",
                    "/api/file/downloadFile/*",
                    "/api/master/verifyTel",
                    "/api/**/seamless/**",
                    "/callback",
                    "/callback/**",
                    "/api/line/callback/**",
                    "/api/core/getprovidergames",
                    "/api/core/gamelogin",
                    "/api/core/verify",
                    "/api/core/bet",
                    "/api/core/win",
                    "/api/core/lose",
                    "/api/core/cancel",
                    "/api/core/userinfo",
                    "/api/core/creditAdd",
                    "/api/core/creditReduce",
                    "/api/core/providerPercent",
//                    "/api/user/promotion",
                    "/v3/api-docs/**",
                    "/swagger-ui.html",
                    "/swagger-ui/**",
                    "/api/master/**",
                    "/api/callback/truewallet/**",

                    "/api/coretransfer/getProviderGames",
                    "/api/coretransfer/gameLogin",
                    "/api/coretransfer/getBalance",
                    "/api/coretransfer/toVendor",
                    "/api/coretransfer/toMain",
                    "/api/coretransfer/fetchTransactions",
                    "/api/coretransfer/importGames",
                    "/api/coretransfer/getIp",

                    "/api/robot/init",
                    "/api/robot/status",
                    "/api/robot/update",

                    "/api/admin/affiliateByUser",

                    "/api/user/getOtp",
                    "/api/user/reqPassword",
                    "/api/user/findGame",
                    "/api/user/lastPlayed",
                    "/api/user/getAllUsersByPrefix"
            ));

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        if (request.getContentType() != null && request.getContentType().contains(MediaType.MULTIPART_FORM_DATA_VALUE)) {
            try {
                Authentication authentication = tokenAuthenticationService.getAuthentication(request);

                SecurityContextHolder.getContext().setAuthentication(authentication);
            } catch (ExpiredJwtException e) {
                logger.warn("the token is expired and not valid anymore in header", e);
            } catch (MalformedJwtException e) {
                logger.warn(e.getMessage(), e);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }

            filterChain.doFilter(request, response);
        } else {
            MultiReadRequest multiReadRequest = new MultiReadRequest(request);
            log.debug("Log header : host  {} : Proto {} : Port {}", multiReadRequest.getHeader("X-Forwarded-Host"),
                    multiReadRequest.getHeader("X-Forwarded-Proto"),
                    multiReadRequest.getHeader("X-Forwarded-Port"));
            Collections.list(multiReadRequest.getHeaderNames()).forEach(s -> {
                log.debug("HEADER -> {} : {} ", s, multiReadRequest.getHeader(s));
            });

            log.debug("Before Filter ");


            try {
                Authentication authentication = tokenAuthenticationService.getAuthentication(multiReadRequest);

//                todo log user and ip to compare
//                UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
//                return userPrincipal.getUsername();


                SecurityContextHolder.getContext().setAuthentication(authentication);
            } catch (ExpiredJwtException e) {
                logger.warn("the token is expired and not valid anymore in header", e);
            } catch (MalformedJwtException e) {
                logger.warn(e.getMessage(), e);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }

            filterChain.doFilter(multiReadRequest, response);
        }


        log.info("After Filter");
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {

        return excludeUrlPatterns.stream()
                .anyMatch(p -> pathMatcher.match(p, request.getServletPath())) || !request.getServletPath().startsWith("/api");
    }

}
