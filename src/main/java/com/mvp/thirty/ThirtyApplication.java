package com.mvp.thirty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThirtyApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThirtyApplication.class, args);
    }

}
