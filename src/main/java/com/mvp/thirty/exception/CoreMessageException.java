package com.mvp.thirty.exception;

import com.mvp.thirty.commons.Constants;
import org.springframework.http.HttpStatus;


public class CoreMessageException extends RuntimeException {

	private static final long serialVersionUID = 2L;

	private Constants.ERROR code;
	private Object params[];
	private HttpStatus httpStatus = HttpStatus.OK;

	public CoreMessageException() {
	}

	public CoreMessageException(Constants.ERROR code, Object...params) {
		this.code = code;
		this.params = params;
	}

	public CoreMessageException(Constants.ERROR code, HttpStatus httpStatus, Object...params) {
		this.code = code;
		this.params = params;
		this.httpStatus = httpStatus;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}
	
	public Constants.ERROR getCode() {
		return code;
	}

	public Object[] getParams() {
		return params;
	}
	
}
