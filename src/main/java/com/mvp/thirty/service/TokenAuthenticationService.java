package com.mvp.thirty.service;


import com.mvp.thirty.payload.commons.UserPrincipal;
import com.mvp.thirty.utils.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;

@Slf4j
@Service
@RequiredArgsConstructor
public class TokenAuthenticationService {

    @Value("#{servletContext.contextPath}")
    private String servletContextPath;

    private final JwtTokenUtil jwtTokenUtil;
    static final String HEADER_STRING = "Authorization";
    static final String HEADER_TOKEN = "Bearer ";


    public Authentication getAuthentication(HttpServletRequest req) {
        String token = getJwtFromRequest(req);
        if (token == null) {
            return null;
        }

        Long userId = Long.valueOf(jwtTokenUtil.getDataToken(token, "userId"));

        String username = jwtTokenUtil.getDataToken(token, "username");

        String agentIdString = jwtTokenUtil.getDataToken(token, "agentId");


        Long agentId = "null".equals(agentIdString) ? null : Long.valueOf(agentIdString);


        String type = jwtTokenUtil.getDataToken(token, "type");

        String prefix = jwtTokenUtil.getDataToken(token, "prefix");

        String ip = req.getHeader("X-Real-IP");

        UserPrincipal principal = new UserPrincipal(userId, username, "", agentId, type, null , ip);
        principal.setPrefix(prefix);


        return new UsernamePasswordAuthenticationToken(principal, null, Collections.emptyList());
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader(HEADER_STRING);

        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith(HEADER_TOKEN)) {
            return bearerToken.substring(6);
        }
        return null;
    }
}

