package com.mvp.thirty.config;

import com.mvp.thirty.exception.CustomizedResponseEntityExceptionHandler;
import com.mvp.thirty.interceptor.JwtAuthorizationTokenFilter;
import com.mvp.thirty.interceptor.LogTransactionFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.StrictHttpFirewall;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthorizationTokenFilter jwtAuthorizationTokenFilter;

    @Autowired
    private CustomizedResponseEntityExceptionHandler exceptionHandler;

    @Autowired
    private LogTransactionFilter logTransactionFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().disable()
                .csrf().disable()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers(
                        "/api/test/**",
                        "/api/master/**",
                        "/api/user/prefix",
                        "/api/user/register",
                        "/api/user/auth",
                        "/api/file/downloadFile/**",
                        "/api/**/seamless/**",
                        "/api/sms/**",
                        "/callback",
                        "/callback/**",
                        "/api/line/callback/**",
                        "/api/core/getprovidergames",
                        "/api/core/gamelogin",
                        "/api/core/verify",
                        "/api/core/bet",
                        "/api/core/win",
                        "/api/core/lose",
                        "/api/core/cancel",
                        "/api/core/userinfo",
                        "/api/core/creditAdd",
                        "/api/core/creditReduce",
                        "/api/core/providerPercent",
//                        "/api/user/promotion",
                        "/v3/api-docs/**",
                        "/swagger-ui.html",
                        "/swagger-ui/**",
                        "/api/master/**",
                        "/api/callback/truewallet/**",
                        "/api/user/livechat",
                        "/noti-websocket/**",
                        "/api/user/seo",
                        "/api/user/footer",
                        "/api/user/icon",
                        "/api/user/chanonical",
                        "/api/user/banner",
                        "/api/user/running-text",
                        "/api/user/footer-text",
                        "/api/user/payment",
                        "/api/user/contact",
                        "/api/user/popular-game",
                        "/api/user/popular-game/**",
                        "/api/user/provider",
                        "/api/user/lastPlayed",
                        "/api/user/getAllUsersByPrefix",

                        "/api/user/transaction-history",

                        "/api/user/getOtp",
                        "/api/user/reqPassword",
                        "/api/user/findGame",

                        "/api/coretransfer/getProviderGames",
                        "/api/coretransfer/gameLogin",
                        "/api/coretransfer/getBalance",
                        "/api/coretransfer/toVendor",
                        "/api/coretransfer/toMain",
                        "/api/coretransfer/fetchTransactions",
                        "/api/coretransfer/importGames",
                        "/api/coretransfer/getIp",

                        "/api/robot/init",
                        "/api/robot/status",
                        "/api/robot/update"
                ).permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .addFilterBefore(logTransactionFilter,
                        UsernamePasswordAuthenticationFilter.class
                )
                .addFilterBefore(
                        jwtAuthorizationTokenFilter,
                        UsernamePasswordAuthenticationFilter.class
                );

        http.exceptionHandling()
                .authenticationEntryPoint(exceptionHandler);
    }

    @Override
    public void configure(WebSecurity web) {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        web.httpFirewall(firewall);
        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**");
        web.ignoring().antMatchers(
                "/api/master/**",
                "/api/test/**",
                "/api/admin/auth"
        );
    }

}

