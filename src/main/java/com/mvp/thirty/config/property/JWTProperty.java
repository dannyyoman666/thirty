package com.mvp.thirty.config.property;


import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

@Data
@Service
@ConfigurationProperties("lotbet.kong.jwt")
public class JWTProperty {

    @Autowired
    private KongService kongService;

    private String url;
    private String port;
    private String workspace;
    private String adminToken;

    /**
     * Set JWT Secret key
     */
    private String secret;

    private String consumer;

    private String key;

    private String algorithm;

    public String getSecret() {
        this.getData();
        return secret;
    }

    public String getKey() {
        this.getData();
        return key;
    }

    public String getAlgorithm() {
        this.getData();
        return algorithm;
    }

    private boolean getData() {
        boolean result = false;
        if (this.secret == null || this.key == null || this.algorithm == null) {
            result = kongService.getSecretKey();
        }
        return result;
    }
}
