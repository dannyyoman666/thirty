package com.mvp.thirty.config.property;


import com.mvp.thirty.commons.Constants;
import com.mvp.thirty.utils.JwtTokenUtil;
import com.mvp.thirty.utils.RestTemplateUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class KongService {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private JWTProperty jwtProperty;


    public Map<String, Object> getTokenCustomer(String username, Constants.Source source, String mflowIP) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("customerID", username);
        claims.put("source", source);
        claims.put("date", LocalDateTime.now().toString());
        claims.put("iss", jwtProperty.getKey());
        if (mflowIP == null) mflowIP = "";
        claims.put("x-mflow", mflowIP);

        Map<String, Object> mapToken = new HashMap<>();
        mapToken.put("accessToken", jwtTokenUtil.generateToken(claims, "customer"));
        return mapToken;
    }

    public Map<String, Object> getTokenAccessKey(Constants.Source source, String mflowIP) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("source", source);
        claims.put("date", LocalDateTime.now().toString());
        claims.put("iss", jwtProperty.getKey());
        claims.put("x-mflow", mflowIP);
        Map<String, Object> mapToken = new HashMap<>();
        mapToken.put("accessKey", jwtTokenUtil.generateToken(claims, "accesskey"));
        return mapToken;
    }


    public boolean getSecretKey() {
        boolean result = false;
        log.info("kong service : start");
        try {
            RestTemplate restTemplate = RestTemplateUtil.restTemplate();


            HttpHeaders headers = new HttpHeaders();
            if (!jwtProperty.getAdminToken().isEmpty())
                headers.set("Kong-Admin-Token", jwtProperty.getAdminToken()); // optional - in case you auth in headers
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

            ResponseEntity<KongJWT> respEntity = restTemplate.exchange("http://{url}:{port}/{workspace}/consumers/{consumer}/jwt", HttpMethod.GET, entity, KongJWT.class,
                    jwtProperty.getUrl(),
                    jwtProperty.getPort(),
                    jwtProperty.getWorkspace(),
                    jwtProperty.getConsumer());


            KongJWT kongJWT = respEntity.getBody();
            if (!kongJWT.getData().isEmpty()) {
                KongJWT.KongJWTData kongData = kongJWT.getData().get(kongJWT.getData().size()-1);

                log.info("kong service : key {} : alg {} : secret {}" ,
                        kongData.getKey(), kongData.getAlgorithm(),
                        kongData.getSecret());

                jwtProperty.setKey(kongData.getKey());
                jwtProperty.setAlgorithm(kongData.getAlgorithm());
                jwtProperty.setSecret(kongData.getSecret());
                result = true;
            }
        } catch (Exception e) {
            //HS256
            jwtProperty.setAlgorithm("HS256");
            log.error("Get secret key", e);
            //  throw new JWTException(e.getMessage());
        }
        log.info("kong service : end");
        return result;
    }

}

