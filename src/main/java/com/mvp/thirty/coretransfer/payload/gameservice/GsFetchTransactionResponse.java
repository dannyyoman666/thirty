package com.mvp.thirty.coretransfer.payload.gameservice;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GsFetchTransactionResponse implements Serializable {
    private List<GsFetchTransactionResponseTransactionObj> transactions;
    private String extraParam;
}
