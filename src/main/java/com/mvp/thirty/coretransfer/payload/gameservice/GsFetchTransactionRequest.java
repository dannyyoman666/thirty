package com.mvp.thirty.coretransfer.payload.gameservice;

import lombok.Data;

import java.io.Serializable;

@Data
public class GsFetchTransactionRequest implements Serializable {
    private Long fromTime;
    private Long toTime;
    private String extraParam;
}
