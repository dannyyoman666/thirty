package com.mvp.thirty.coretransfer.payload.coretransferservice;

import lombok.Data;
import org.apache.kafka.common.protocol.types.Field;

import java.io.Serializable;

@Data
public class CtsFetchTransactionResponse implements Serializable {
    private String provider;
    private Boolean isSuccess;
    private Integer totalRowFetched;
    private String extraParam;
}