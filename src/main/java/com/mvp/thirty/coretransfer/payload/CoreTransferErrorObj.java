package com.mvp.thirty.coretransfer.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CoreTransferErrorObj {
    private String code;
    private String description;
}
