package com.mvp.thirty.coretransfer.payload.coretransferservice;

import lombok.Data;
import java.io.Serializable;

@Data
public class CtsFetchTransactionRequest implements Serializable {
    private String provider;
    private Long fromTime;
    private Long toTime;
    private String extraParam;
}