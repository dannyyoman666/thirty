package com.mvp.thirty.coretransfer.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CoreTransferApiResponse<T> implements Serializable {
    private Boolean status;
    private String message;
    private T data;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    List<CoreTransferErrorObj> errors;
}
