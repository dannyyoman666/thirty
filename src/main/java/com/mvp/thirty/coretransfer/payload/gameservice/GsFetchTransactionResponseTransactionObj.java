package com.mvp.thirty.coretransfer.payload.gameservice;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class GsFetchTransactionResponseTransactionObj implements Serializable {
    private String provider;
    private String username;
    private String gameId;
    private String gameCode;
    private String gameType;
    private String gameTransactionId;
    private BigDecimal stake;
    private BigDecimal payout;
    private BigDecimal balance;
    private Long gameStartTime;
    private Long gameEndTime;
    private String status;
    private String detail;
}
