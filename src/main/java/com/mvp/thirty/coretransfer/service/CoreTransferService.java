package com.mvp.thirty.coretransfer.service;

import com.mvp.thirty.coretransfer.payload.*;
import com.mvp.thirty.coretransfer.payload.coretransferservice.*;
import org.springframework.stereotype.Service;

@Service
public interface CoreTransferService {
    CtsFetchTransactionResponse fetchTransactions(CtsFetchTransactionRequest request);
}