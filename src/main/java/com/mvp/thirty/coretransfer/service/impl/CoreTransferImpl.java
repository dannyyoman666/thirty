package com.mvp.thirty.coretransfer.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mvp.thirty.commons.Constants;
import com.mvp.thirty.coretransfer.payload.*;
import com.mvp.thirty.coretransfer.payload.coretransferservice.*;
import com.mvp.thirty.coretransfer.payload.gameservice.*;
import com.mvp.thirty.coretransfer.service.CoreTransferService;
import com.mvp.thirty.exception.CoreMessageException;
import com.mvp.thirty.postgres.entity.GameProvider;
import com.mvp.thirty.postgres.repository.GameProviderRepository;
import com.mvp.thirty.utils.RestTemplateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.Optional;

@Service
public class CoreTransferImpl implements CoreTransferService {
    @Autowired
    private GameProviderRepository gameProviderRepository;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public CtsFetchTransactionResponse fetchTransactions(CtsFetchTransactionRequest request) {
        Optional<GameProvider> opt = gameProviderRepository.findByProvider(request.getProvider());
        if (!opt.isPresent())
            throw new CoreMessageException(Constants.ERROR.CORE_01001);
        else if (request.getProvider().isEmpty())
            throw new CoreMessageException(Constants.ERROR.CORE_01002);

        try {
            String url = opt.get().getCoreUrl() + "/transaction/fetch";
            RestTemplate restTemplate = RestTemplateUtil.restTemplate();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            GsFetchTransactionRequest gsRequest = new GsFetchTransactionRequest();
            gsRequest.setFromTime(request.getFromTime());
            gsRequest.setToTime(request.getToTime());
            gsRequest.setExtraParam(request.getExtraParam());

            System.out.println("Core Fetch Transactions Url : "+ url);
            System.out.println("Core Fetch Transactions Request : "+ gsRequest);
            HttpEntity<CtsFetchTransactionRequest> req = new HttpEntity<>(request, headers);
            ResponseEntity<String> res = restTemplate.postForEntity(url, req, String.class);
            CoreTransferApiResponse<GsFetchTransactionResponse> gsResponse = objectMapper.readValue(res.getBody(), new TypeReference<CoreTransferApiResponse<GsFetchTransactionResponse>>() {});
            System.out.println("Fetch Transactions response : "+ gsResponse);

            CtsFetchTransactionResponse response = new CtsFetchTransactionResponse();
            response.setProvider(opt.get().getProvider());
            response.setIsSuccess(false);

            if (gsResponse.getStatus()){
                GsFetchTransactionResponse gsResponseData = gsResponse.getData();

                for (GsFetchTransactionResponseTransactionObj transaction : gsResponseData.getTransactions()) {
                    transaction.setProvider(opt.get().getProvider());
                    kafkaTemplate.send("transactionTopic", objectMapper.writeValueAsString(transaction));
                }

                response.setIsSuccess(true);
                response.setTotalRowFetched(gsResponseData.getTransactions().size());
                response.setExtraParam(gsResponseData.getExtraParam());
            }

            return response;
        } catch (Exception e) {
            throw new CoreMessageException(Constants.ERROR.CORE_00001);
        }
    }
    
}