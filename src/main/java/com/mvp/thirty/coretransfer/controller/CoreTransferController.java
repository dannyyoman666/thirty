package com.mvp.thirty.coretransfer.controller;

import com.mvp.thirty.commons.Constants;
import com.mvp.thirty.coretransfer.payload.coretransferservice.*;
import com.mvp.thirty.coretransfer.service.CoreTransferService;
import com.mvp.thirty.utils.ResponseHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

@RestController
@RequestMapping(value = "/api/coretransfer")
public class CoreTransferController {
    @Autowired
    private CoreTransferService coreTransferService;

    @PostMapping(value = "/fetchTransactions",
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> fetchTransactions(@RequestBody CtsFetchTransactionRequest request) {
        CtsFetchTransactionResponse result = coreTransferService.fetchTransactions(request);
        return ResponseHelper.successWithData(Constants.MESSAGE.MSG_00000.msg, result);
    }
}