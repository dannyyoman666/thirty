package com.mvp.thirty.postgres.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mvp.thirty.postgres.entity.audit.DateAudit;
import com.mvp.thirty.postgres.entity.audit.UserAuditEmbeddable;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "game_provider", uniqueConstraints = {@UniqueConstraint(columnNames = {"provider", "category"})})
@Where(clause = "delete_flag = 0")
public class GameProvider extends DateAudit<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "provider", columnDefinition = "character varying(50)")
    private String provider;

    @Column(name = "gameName", columnDefinition = "character varying(50)")
    private String gameName;

    @Column(name = "image", columnDefinition = "character varying(200)")
    private String image;

    @Column(name = "category", columnDefinition = "character varying(50)")
    private String category;

    @Column(name = "type", columnDefinition = "character varying(50)")
    private String type;

    @Column(name = "core_url", columnDefinition = "character varying(100)")
    private String coreUrl;

    @Column(name = "is_sport")
    private Boolean isSport;
    @Column(name = "is_live")
    private Boolean isLive;
    @Column(name = "is_slot")
    private Boolean isSlot;
    @Column(name = "is_fishing")
    private Boolean isFishing;
    @Column(name = "is_card")
    private Boolean isCard;

    @Column(name = "active", columnDefinition = "boolean")
    private Boolean active;

    @Column(name = "is_amb", columnDefinition = "boolean")
    private Boolean isAmb;

    @Column(name = "maximum_percent")
    private BigDecimal maximumPercent;

    @Embedded
    private UserAuditEmbeddable audit = new UserAuditEmbeddable();

}
