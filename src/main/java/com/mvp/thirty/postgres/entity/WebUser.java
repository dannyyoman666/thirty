package com.mvp.thirty.postgres.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mvp.thirty.postgres.entity.audit.DateAudit;
import com.mvp.thirty.postgres.entity.audit.UserAuditEmbeddable;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"username"}) ,
        @UniqueConstraint(columnNames = {"account_number", "agent_id"})})
@Where(clause = "delete_flag = 0")
public class WebUser extends DateAudit<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // user for web
    @Column(name = "username", columnDefinition = "character varying(50)")
    private String username;

    @Column(name = "password", columnDefinition = "character varying(500)")
    private String password;

    @Column(name = "tel", columnDefinition = "character varying(50)")
    private String tel;

    @Column(name = "bank_name", columnDefinition = "character varying(50)")
    private String bankName;

    @Column(name = "account_number", columnDefinition = "character varying(50)")
    private String accountNumber;

    @Column(name = "first_name", columnDefinition = "character varying(50)")
    private String firstName;

    @Column(name = "last_name", columnDefinition = "character varying(50)")
    private String lastName;

    @Column(name = "line", columnDefinition = "character varying(50)")
    private String line;

    @Column(name = "nationality", columnDefinition = "character varying(15)")
    private String nationality;

    @Column(name = "is_bonus", columnDefinition = "character varying(50)")
    private String isBonus;

    @Column(name = "block_bonus", columnDefinition = "boolean default false")
    private Boolean blockBonus;

    @Column(name = "deposit_auto", columnDefinition = "boolean default true")
    private Boolean depositAuto;

    @Column(name = "withdraw_auto", columnDefinition = "boolean default true")
    private Boolean withdrawAuto;

    @Column(name = "deposit_ref")
    private String depositRef;

    @Column(name = "withdraw_limit", columnDefinition = "integer default 0")
    private Integer withdrawLimit = 0;

    @Column(name = "line_open_id", columnDefinition = "character varying(50)")
    private String lineOpenId;

    @Column(name = "select_promotion")
    private Long selectPromotion;

    @Column(name = "marketing_id")
    private Long marketingId;

    @Column(name = "vip", columnDefinition = "integer default 1")
    private Long vip;

    @Column(name = "last_active_ip")
    private String lastActiveIp;

    @Column(name = "commission_valid_flag")
    private Boolean commissionValidFlag;

    @ToString.Exclude
    @JsonIgnore
    @JoinColumn(name = "member_level_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MemberLevel memberLevel;

    @ToString.Exclude
    @JsonIgnore
    @JoinColumn(name = "member_tags_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MemberTags memberTags;

    @Embedded
    private UserAuditEmbeddable audit = new UserAuditEmbeddable();

    @JsonIgnore
    @ToString.Exclude
    @JoinColumn(name = "agent_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Agent agent;

    @OneToOne(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    private Wallet wallet;

    @ToString.Exclude
    @OneToMany(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    private List<AffiliateUser> affiliateUsers;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    private List<DepositHistory> depositHistory;

    @OneToMany(mappedBy = "user", cascade = {CascadeType.MERGE, CascadeType.PERSIST}, fetch = FetchType.LAZY)
    private List<PromotionHistory> promotionHistory;

}
