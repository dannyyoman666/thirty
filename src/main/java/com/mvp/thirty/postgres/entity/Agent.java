package com.mvp.thirty.postgres.entity;

import com.mvp.thirty.postgres.entity.audit.DateAudit;
import com.mvp.thirty.postgres.entity.audit.UserAuditEmbeddable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "agent", uniqueConstraints = {@UniqueConstraint(columnNames = {"prefix"})})
@Where(clause = "delete_flag = 0")
public class Agent extends DateAudit<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @JsonIgnore
//    @ToString.Exclude
//    @JoinColumn(name = "senior_id", referencedColumnName = "id")
//    @ManyToOne(fetch = FetchType.LAZY)
//    private SeniorMaster seniorMaster;
//
//    @Where(clause = "type = 'AGENT-CONFIG'")
//    @OneToMany(mappedBy = "agent", cascade = {CascadeType.MERGE,CascadeType.PERSIST}, fetch = FetchType.LAZY)
//    private List<Config> gameConfig;

    // for web prefix
    @NotNull
    @Column(name = "prefix", columnDefinition = "character varying(50)")
    private String prefix;

    @Column(name = "website", columnDefinition = "character varying(50)")
    private String website;

    @Column(name = "line_id", columnDefinition = "character varying(50)")
    private String lineId;

    @Column(name = "logo", columnDefinition = "character varying(500)")
    private String logo;

    @Column(name = "background", columnDefinition = "character varying(500)")
    private String background;

    @Column(name = "line_token", columnDefinition = "character varying(500)")
    private String lineToken;

    @Column(name = "line_token_withdraw", columnDefinition = "character varying(500)")
    private String lineTokenWithdraw;
    @Column(name = "telegram_token", columnDefinition = "character varying(200)")
    private String telegramToken;
    @Column(name = "telegram_deposit", columnDefinition = "character varying(200)")
    private String telegramDeposit;
    @Column(name = "telegram_withdraw", columnDefinition = "character varying(200)")
    private String telegramWithdraw;
    @Column(name = "credit", columnDefinition = "numeric(18,2)")
    private BigDecimal credit;

    @Embedded
    private UserAuditEmbeddable audit = new UserAuditEmbeddable();

    @ToString.Exclude
    @OneToMany(mappedBy = "agent", cascade = {CascadeType.MERGE,CascadeType.PERSIST} , fetch = FetchType.LAZY)
    private List<WebUser> webUsers;

    @ToString.Exclude
    @OneToMany(mappedBy = "agent", cascade = {CascadeType.MERGE,CascadeType.PERSIST}, fetch = FetchType.LAZY)
    private List<AdminUser> adminUsers;

    @Where(clause = "type = 'AGENT-CONFIG'")
    @OneToMany(mappedBy = "agent", cascade = {CascadeType.MERGE,CascadeType.PERSIST}, fetch = FetchType.LAZY)
    private List<Config> configs;

    @ToString.Exclude
    @OneToMany(mappedBy = "agent", cascade = {CascadeType.MERGE,CascadeType.PERSIST} , fetch = FetchType.LAZY)
    private List<Promotion> promotions;

    @ToString.Exclude
    @OneToMany(mappedBy = "agent", cascade = {CascadeType.MERGE,CascadeType.PERSIST} , fetch = FetchType.LAZY)
    private List<PromotionHistory> promotionHistory ;

    @Column(name = "backgroundwallet_img", columnDefinition = "character varying(200)")
    private String backgroundwalletImg;

    @Column(name = "menumember_img", columnDefinition = "character varying(200)")
    private String menumemberImg;
    @Column(name = "withdraw_img", columnDefinition = "character varying(200)")
    private String withdrawImg;
    @Column(name = "deposit_img", columnDefinition = "character varying(200)")
    private String depositImg;
    @Column(name = "contact_img", columnDefinition = "character varying(200)")
    private String contactImg;
    @Column(name = "register_img", columnDefinition = "character varying(200)")
    private String registerImg;
    @Column(name = "menufinance_img", columnDefinition = "character varying(200)")
    private String menufinanceImg;
    @Column(name = "pointexchange_img", columnDefinition = "character varying(200)")
    private String pointexchangeImg;
    @Column(name = "promotion_img", columnDefinition = "character varying(200)")
    private String promotionImg;
    @Column(name = "pointearn_img", columnDefinition = "character varying(200)")
    private String pointearnImg;

    @Column(name = "wallet_color_code", columnDefinition = "character varying(200)")
    private String walletColorCode;

    @Column(name = "wallet_color2_code", columnDefinition = "character varying(200)")
    private String walletColor2Code;

    @Column(name = "backgroundmoney_img", columnDefinition = "character varying(200)")
    private String backgroundMoneyImg;

}
