package com.mvp.thirty.postgres.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mvp.thirty.postgres.entity.audit.DateAudit;
import com.mvp.thirty.postgres.entity.audit.UserAuditEmbeddable;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "member_level")
@Where(clause = "delete_flag = 0")
public class MemberLevel extends DateAudit<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Basic
    @Column(name = "level_name")
    private String levelName;
    @Basic
    @Column(name = "seq_level")
    private String seqLevel;
    @Basic
    @Column(name = "deposit_count")
    private BigDecimal depositCount;
    @Basic
    @Column(name = "min_total_deposit")
    private BigDecimal minTotalDeposit;
    @Basic
    @Column(name = "min_total_turnover")
    private BigDecimal minTotalTurnover;
    @Basic
    @Column(name = "validity")
    private BigDecimal validity;

    @Embedded
    private UserAuditEmbeddable audit = new UserAuditEmbeddable();

    @ToString.Exclude
    @JsonIgnore
    @JoinColumn(name = "agent_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private Agent agent;


}
