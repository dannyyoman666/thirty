package com.mvp.thirty.postgres.entity;

import com.mvp.thirty.postgres.entity.audit.DateAudit;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "balance_transfer_log")
@Where(clause = "delete_flag = 0")
public class BalanceTransferLog extends DateAudit<String> implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", columnDefinition = "character varying(50) not null")
    private String username;

    @Column(name = "amount", columnDefinition = "numeric(18,2)")
    private BigDecimal amount;

    @Column(name = "balance", columnDefinition = "numeric(18,2)")
    private BigDecimal balance;

    @Column(name = "source", columnDefinition = "character varying(50) not null")
    private String source;

    @Column(name = "target", columnDefinition = "character varying(50) not null")
    private String target;

    @Column(name = "transaction_id", columnDefinition = "character varying(50) not null")
    private String transactionId;

    @Column(name = "reference_id", columnDefinition = "character varying(50) not null")
    private String referenceId;

    @Column(name = "message", columnDefinition = "character varying(200)")
    private String message;
}
