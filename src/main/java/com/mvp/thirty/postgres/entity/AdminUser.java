package com.mvp.thirty.postgres.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mvp.thirty.postgres.entity.audit.DateAudit;
import com.mvp.thirty.postgres.entity.audit.UserAuditEmbeddable;
import lombok.Data;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "admins", uniqueConstraints = {@UniqueConstraint(columnNames = {"username", "agent_id"})})
@Where(clause = "delete_flag = 0")
public class AdminUser extends DateAudit<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", columnDefinition = "character varying(50) not null")
    private String username;

    @Column(name = "password", columnDefinition = "character varying(500) not null")
    private String password;

    @Column(name = "password_no_encrypt", columnDefinition = "character varying(50)")
    private String passwordNoEncrypt;

    @Column(name = "tel", columnDefinition = "character varying(50)")
    private String tel;

    @Column(name = "full_name", columnDefinition = "character varying(50) not null")
    private String fullName;

    @Column(name = "limit_flag", columnDefinition = "smallint default 0")
    private int limitFlag = 0;

    @Column(name = "limit_value", columnDefinition = "decimal")
    private Integer limit = 0;

    @Column(name = "active", columnDefinition = "smallint default 0")
    private int active;

    @Column(name = "prefix", columnDefinition = "character varying(50)")
    private String prefix;

    @Column(name = "last_login_token", columnDefinition = "character varying(50)")
    private String lastLoginToken;

    @Column(name = "mistake_limit")
    private Integer mistakeLimit = 0;

    @Embedded
    private UserAuditEmbeddable audit = new UserAuditEmbeddable();

    @JsonIgnore
    @JoinColumn(name = "agent_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Agent agent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    private RoleConfig role;

    @OneToMany(mappedBy = "admin", cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    private List<DepositHistory> depositHistory;

    @OneToMany(mappedBy = "admin", cascade = {CascadeType.MERGE,CascadeType.PERSIST})
    private List<Promotion> promotion;


}
