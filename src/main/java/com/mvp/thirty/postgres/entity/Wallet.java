package com.mvp.thirty.postgres.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mvp.thirty.postgres.entity.audit.DateAudit;
import com.mvp.thirty.postgres.entity.audit.UserAuditEmbeddable;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "wallet")
@Where(clause = "delete_flag = 0")
public class Wallet extends DateAudit<String> implements Serializable {

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "credit", columnDefinition = "numeric(18,2)")
    private BigDecimal credit;

    @Column(name = "point", columnDefinition = "numeric(18,5)")
    private BigDecimal point;

    @Column(name = "commission", columnDefinition = "numeric(18,5)")
    private BigDecimal commission;

    @Column(name = "turn_over", columnDefinition = "numeric(18,2)")
    private BigDecimal turnOver;

    @Column(name = "min_credit_deposit", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal minCreditDeposit;

    @Column(name = "max_withdraw", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal maxWithdraw;

    @Column(name = "turn_over_all", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverAll;
    @Column(name = "turn_over_football", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverFootball;
    @Column(name = "turn_over_step", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverStep;
    @Column(name = "turn_over_parlay", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverParlay;
    @Column(name = "turn_over_game", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverGame;
    @Column(name = "turn_over_casino", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverCasino;
    @Column(name = "turn_over_lotto", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverLotto;
    @Column(name = "turn_over_m2", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverM2;
    @Column(name = "turn_over_multi_player", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverMultiPlayer;
    @Column(name = "turn_over_poker", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverPoker;
    @Column(name = "turn_over_e_sport", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverESport;
    @Column(name = "turn_over_keno", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverKeno;
    @Column(name = "turn_over_trading", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverTrading;

    @Column(name = "turn_over_sport", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverSport;
    @Column(name = "turn_over_tablegame", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverTableGame;

    @Column(name = "turn_over_fishing", columnDefinition = "numeric(18,2) default 0")
    private BigDecimal turnOverFishing;

    @JsonIgnore
    @ToString.Exclude
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private WebUser user;

    @JsonIgnore
    @ToString.Exclude
    @JoinColumn(name = "deposit_bank_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Bank bank;

    @JsonIgnore
    @ToString.Exclude
    @JoinColumn(name = "deposit_true_wallet_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TrueWallet trueWallet;

    @Embedded
    private UserAuditEmbeddable audit = new UserAuditEmbeddable();
}