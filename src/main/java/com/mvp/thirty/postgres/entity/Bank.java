package com.mvp.thirty.postgres.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mvp.thirty.postgres.entity.audit.DateAudit;
import com.mvp.thirty.postgres.entity.audit.UserAuditEmbeddable;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
@Table(name = "bank")
@Where(clause = "delete_flag = 0")
public class Bank extends DateAudit<String> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "bank_code", columnDefinition = "character varying(50) not null")
    private String bankCode;

    @Column(name = "bank_type", columnDefinition = "character varying(50) not null")
    private String bankType;

    @Column(name = "bank_name", columnDefinition = "character varying(50) not null")
    private String bankName;

    @Column(name = "bank_account_name", columnDefinition = "character varying(60) not null")
    private String bankAccountName;

    @Column(name = "bank_account_no", columnDefinition = "character varying(50) not null")
    private String bankAccountNo;

    @Column(name = "username", columnDefinition = "character varying(50) not null")
    private String username;

    @Column(name = "password", columnDefinition = "character varying(50) not null")
    private String password;

    @Column(name = "bank_order", columnDefinition = "smallint not null")
    private int bankOrder;

    @Column(name = "bank_group", columnDefinition = "smallint not null")
    private int bankGroup;

    @Column(name = "bot_ip", columnDefinition = "character varying(50)")
    private String botIp;

    @Column(name = "new_user_flag", columnDefinition = "boolean")
    private boolean newUserFlag;

    @Column(name = "active", columnDefinition = "boolean")
    private boolean active;

    @Column(name = "prefix", columnDefinition = "character varying(50)")
    private String prefix;

    @Column(name = "nationality", columnDefinition = "character varying(50)")
    private String nationality;

    @Column(name = "currency", columnDefinition = "character varying(10)")
    private String currency;

    @Column(name = "rate")
    private BigDecimal rate;

    @Column(name = "description_img", columnDefinition = "character varying(200)")
    private String descriptionImg;

    @Column(name = "admin_subsidi")
    private BigDecimal adminSubsidi;

    @Column(name = "admin_fee")
    private BigDecimal adminFee;

    @Column(name = "admin_subsidi_percentage")
    private BigDecimal adminSubsidiPercentage;

    @Column(name = "admin_fee_percentage")
    private BigDecimal adminFeePercentage;

    @Column(name = "min_amount")
    private BigDecimal minAmount;

    @Column(name = "max_amount")
    private BigDecimal maxAmount;


//    @Column(name = "member_level_id")
//    private BigDecimal memberLevelId;

    @Embedded
    private UserAuditEmbeddable audit = new UserAuditEmbeddable();

    @ToString.Exclude
    @JsonIgnore
    @JoinColumn(name = "agent_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private Agent agent;

    @OneToMany(mappedBy = "bank", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<Wallet> wallet;


    @OneToMany(mappedBy = "bank", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private List<DepositHistory> depositHistory;

    @ToString.Exclude
    @JsonIgnore
    @JoinColumn(name = "member_level_id", referencedColumnName = "id")
    @OneToOne(fetch = FetchType.LAZY)
    private MemberLevel memberLevel;



}
