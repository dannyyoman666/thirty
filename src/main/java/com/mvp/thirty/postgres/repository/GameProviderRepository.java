package com.mvp.thirty.postgres.repository;

import com.mvp.thirty.postgres.entity.GameProvider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GameProviderRepository extends JpaRepository<GameProvider, Long>, JpaSpecificationExecutor<GameProvider> {

    @Query(value = "select * from game_provider where provider = :provider", nativeQuery = true)
    Optional<GameProvider> findByProvider(String provider);
    List<GameProvider> findByActive(Boolean active);


}
